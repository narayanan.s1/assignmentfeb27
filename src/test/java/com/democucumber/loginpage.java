package com.democucumber;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class loginpage {
	
	WebDriver driver;//instance variable

	public loginpage(WebDriver driver)//local variable
	{
		this.driver=driver;//this.driver is instance variable
	}
	@FindBy(xpath="//a[text()='Log in']")
	private WebElement firstlogin;
	public void getFirstlogin() {
		 firstlogin.click();
	}
	@FindBy(id="Email")
	private WebElement email;
	@FindBy(id="Password")
	private WebElement password;
	@FindBy(xpath="//input[@value='Log in']")
	private WebElement login;

	
	public void getEmail(String s) {
	email.sendKeys(s);

	}
	public void getPassword(String s1) {
		password.sendKeys(s1);
	}
	public void getLogin() {
		login.click();
	}



}
