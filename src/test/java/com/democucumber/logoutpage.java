package com.democucumber;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class logoutpage {
	
	WebDriver driver;//instance variable

	public logoutpage(WebDriver driver)//local variable
	{
		this.driver=driver;//this.driver is instance variable
	}
	@FindBy(linkText="Log out")
	private WebElement clicklogout;
	
	public void clickLogout()
	{
		clicklogout.click();
	}


}
