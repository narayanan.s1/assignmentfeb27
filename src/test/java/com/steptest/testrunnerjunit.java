package com.steptest;

import org.junit.runner.RunWith;

import io.cucumber.testng.CucumberOptions;

@RunWith(cucumber.class)
@CucumberOptions(features = "src/test/resources/feature/demo.feature"
,glue="stepTest",plugin = { "pretty", "html:target/cucumber-re" })
public class testrunnerjunit {

}
