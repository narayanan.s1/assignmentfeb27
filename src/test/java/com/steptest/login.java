package com.steptest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import com.democucumber.Giftcard;
import com.democucumber.bookpage;
import com.democucumber.electronics;
import com.democucumber.loginpage;
import com.democucumber.logoutpage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class login {
	static WebDriver driver=null;

	
	@Given("user have to enter DemoWebShop application through chrome browser")
	public void user_have_to_enter_demo_web_shop_application_through_chrome_browser() {
		driver = new ChromeDriver();
	    driver.get("https://demowebshop.tricentis.com/");
	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    driver.manage().window().maximize();
	    
	}

	@When("click on the login button")
	public void click_on_the_login_button() {
		loginpage lp=PageFactory.initElements(driver,loginpage.class);
		lp.getFirstlogin();
	    
	}

	@When("user have to enter valid {string} and valid {string}")
	public void user_have_to_enter_valid_and_valid(String string, String string2) {
		loginpage lp=PageFactory.initElements(driver,loginpage.class);
		//driver.findElement(By.id("Email")).sendKeys(string);
		// driver.findElement(By.id("Password")).sendKeys(string);
	   lp.getEmail(string);
	   lp.getPassword(string2);
	
	    
	}

	@Then("user have to click login button")
	public void user_have_to_click_login_button() throws InterruptedException {
		loginpage lp=PageFactory.initElements(driver,loginpage.class);
		Thread.sleep(3000);
		lp.getLogin();
	    
	}

	@Then("user have to get title of the home page")
	public void user_have_to_get_title_of_the_home_page() {
		String title = driver.getTitle();
        System.out.println(title);
    
	    
	}

	@Given("user have to click book category")
	public void user_have_to_click_book_category() {
		bookpage bp=PageFactory.initElements(driver,bookpage.class);
		bp.clickbooks();
	    
	    
	}

	@When("user have to  sort price high to low")
	public void user_have_to_sort_price_high_to_low() {
		bookpage bp=PageFactory.initElements(driver,bookpage.class);
		bp.sort();
	    
	}

	@Then("user have to Add books in add to cart")
	public void user_have_to_add_books_in_add_to_cart() throws InterruptedException {
		bookpage bp=PageFactory.initElements(driver,bookpage.class);
		bp.addToCart1();
		Thread.sleep(3000);
		bp.addToCart2();
	    
	}

	@Given("user have to click Electronic category")
	public void user_have_to_click_electronic_category() throws InterruptedException {
		electronics ep=PageFactory.initElements(driver,electronics.class);
		Thread.sleep(3000);
		ep.clickelectronics();
	    
	}

	@When("user have to select cellphone")
	public void user_have_to_select_cellphone() {
		electronics ep=PageFactory.initElements(driver,electronics.class);
		ep.cellphone();
	    
	}

	@Then("user have to add any product in add to cart")
	public void user_have_to_add_any_product_in_add_to_cart() {
		electronics ep=PageFactory.initElements(driver,electronics.class);
		ep.addToCart1();
	    
	}

	@Then("user have to display count of items present in shopping cart")
	public void user_have_to_display_count_of_items_present_in_shopping_cart() throws InterruptedException {
		electronics ep=PageFactory.initElements(driver,electronics.class);
		ep.shopingCart1();
	    
	}

	@Given("user have to click on  gift card category")
	public void user_have_to_click_on_gift_card_category() throws InterruptedException {
		Giftcard gp=PageFactory.initElements(driver,Giftcard.class);
		Thread.sleep(3000);
		gp.clickGiftCard();
	    
	}

	@When("user have to select the four  giftcards perpage")
	public void user_have_to_select_the_four_giftcards_perpage() {
		Giftcard gp=PageFactory.initElements(driver,Giftcard.class);
		gp.selectGiftCard();   
	}

	@When("user have to capture any one of the giftcard name and price")
	public void user_have_to_capture_any_one_of_the_giftcard_name_and_price() {
		Giftcard gp=PageFactory.initElements(driver,Giftcard.class);
		gp.displayGiftCart();   
	}

	@Given("user have to logout the application")
	public void user_have_to_logout_the_application() throws InterruptedException {
		logoutpage log=PageFactory.initElements(driver,logoutpage.class);
		Thread.sleep(3000);
		log.clickLogout();
	    
	}

	@Then("login button should be displayed  on the homepage")
	public void login_button_should_be_displayed_on_the_homepage() {
		if (driver.findElement(By.linkText("Log in")).isDisplayed()) {
            System.out.println("login is displayed");
        } else {
            System.out.println("login is not displayed");
        }
	   
	   
	}


}
