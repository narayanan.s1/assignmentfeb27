package com.steptest;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(tags = "", features ="src/test/resources/feature/demo.feature",
glue = "stepTest",
plugin = { "pretty", "html:target/cucumber-reportss" },
monochrome = true)

public class TestRunner extends AbstractTestNGCucumberTests{

}
